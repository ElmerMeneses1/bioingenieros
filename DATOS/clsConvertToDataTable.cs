﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATOS
{
    public class clsConvertToDataTable
    {
        private static clsConvertToDataTable instancia;

        public static clsConvertToDataTable Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new clsConvertToDataTable();
                }
                return instancia;
            }
        }
    }
}