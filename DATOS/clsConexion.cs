﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace DATOS
{
    public class clsConexion
    {
        public SqlConnection cn = new SqlConnection("server=SQL8002.site4now.net; database=db_a88f40_bioinge; uid=db_a88f40_bioinge_admin; pwd=S0p0rte!");
        //public SqlConnection cn = new SqlConnection("server=MELGAREJOSAC\\SQLEXPRESS; database=SistemaMunicipal; uid=sa; pwd=sql");

        private static clsConexion instancia;

        public static clsConexion Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new clsConexion();
                }
                return instancia;
            }
        }

        public void Conectar()
        {
            if (cn.State == ConnectionState.Closed)
            {
                cn.Open();
            }

            return;
        }

        public void Desconectar()
        {
            if (cn.State == ConnectionState.Open)
            {
                cn.Close();
            }

            return;
        }

        public DataTable Listado(String NombreSP, List<clsParametro> lst)
        {
            using (var dt = new DataTable())
            {
                using(var da = new SqlDataAdapter(NombreSP, cn))
                {
                    try
                    {
                        Conectar();
                        da.SelectCommand.CommandTimeout = 0;

                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        if (lst != null)
                        {
                            for (int i = 0; i < lst.Count; i++)
                            {
                                da.SelectCommand.Parameters.AddWithValue(lst[i].Nombre, lst[i].Valor);
                            }
                        }
                        da.Fill(dt);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        Desconectar();
                    }
                }
                return dt;
            }
        }

        public void EjecutarSP(String NombreSP, ref List<clsParametro> lst)
        {
            using (SqlCommand cmd = new SqlCommand(NombreSP, cn))
            {
                try
                {
                    Conectar();
                    cmd.CommandTimeout = 0;

                    cmd.CommandType = CommandType.StoredProcedure;

                    if (lst != null)
                    {
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (lst[i].Direccion == ParameterDirection.Input && lst[i].TipoDato != SqlDbType.Structured)
                            {
                                cmd.Parameters.AddWithValue(lst[i].Nombre, lst[i].Valor);
                            }
                            if (lst[i].Direccion == ParameterDirection.Output)
                            {
                                cmd.Parameters.Add(lst[i].Nombre, lst[i].TipoDato, lst[i].Tamaño).Direction = ParameterDirection.Output;
                            }
                            if (lst[i].TipoDato == SqlDbType.Structured)
                            {
                                cmd.Parameters.AddWithValue(lst[i].Nombre, lst[i].Valor).SqlDbType = SqlDbType.Structured;
                            }
                        }
                        cmd.ExecuteNonQuery();
                        for (int i = 0; i < lst.Count; i++)
                        {
                            if (cmd.Parameters[i].Direction == ParameterDirection.Output)
                            {
                                lst[i].Valor = cmd.Parameters[i].Value;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    Desconectar();
                }
            }
        }
    }
}
