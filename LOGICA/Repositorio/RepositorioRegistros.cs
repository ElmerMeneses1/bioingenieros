﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATOS;
using LOGICA.Interface;

namespace LOGICA.Repositorio
{
    public class RepositorioRegistros : IRepositorioRegistros
    {
        public string RegistrarUsuario(string nombre, string apellido, string email, string usuario, string clave, int pais)
        {

            string mensaje = "";

            List<clsParametro> lst = new List<clsParametro>();

            try
            {
                lst.Add(new clsParametro("@nombre", nombre));
                lst.Add(new clsParametro("@apellido", apellido));
                lst.Add(new clsParametro("@email", email));
                lst.Add(new clsParametro("@usuario", usuario));
                lst.Add(new clsParametro("@clave", clave));
                lst.Add(new clsParametro("@pais", pais));
                lst.Add(new clsParametro("@mensaje", "", SqlDbType.VarChar, ParameterDirection.Output,200));
                clsConexion.Instancia.EjecutarSP("sp_RegistrarUsuario", ref lst);
                mensaje = lst[6].Valor.ToString();
            }

            catch (Exception ex)
            {

                throw ex;
            }

            return mensaje;
        }
    }
}
