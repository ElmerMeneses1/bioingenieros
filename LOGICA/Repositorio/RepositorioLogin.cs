﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LOGICA.Interface;
using DATOS;
using System.Data;

namespace LOGICA.Repositorio
{
    public class RepositorioLogin : IRepositorioLogin
    {
        public string EliminarIP(string usuario)
        {
            List<clsParametro> lst = new List<clsParametro>();
            String Mensaje = "";
            try
            {
                lst.Add(new clsParametro("@usuario", usuario));
                clsConexion.Instancia.EjecutarSP("sp_EliminarIP", ref lst);
                Mensaje = "Respeto guardan Respeto.";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Mensaje;
        }

        public string IniciarSession(string usuario, string clave, string ip)
        {
            List<clsParametro> lst = new List<clsParametro>();

            string mensaje = "";

            try
            {
                lst.Add(new clsParametro("@usuario", usuario));
                lst.Add(new clsParametro("@clave", clave));
                lst.Add(new clsParametro("@ipa", ip));
                lst.Add(new clsParametro("@mensaje", "", SqlDbType.VarChar , ParameterDirection.Output,200));
                clsConexion.Instancia.EjecutarSP("sp_Login", ref lst);
                mensaje = lst[3].Valor.ToString();
            }
            catch (Exception ex)
            {

                throw ex ;
            }
         

            return mensaje;
        }
    }
}
