﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DATOS;
using LOGICA.Interface;
using LOGICA.Modelo;

namespace LOGICA.Repositorio
{
    public class RepositorioListado : IRepositorioListados
    {
        public List<ModeloComboPais> ComboPais()
        {
            List<clsParametro> lst = new List<clsParametro>();
            try
            {

           
                return clsConexion.Instancia.Listado("sp_pais", lst).AsEnumerable().Select(row => new ModeloComboPais
                {
                    id = Convert.ToInt32(row[0]),
                    valor = Convert.ToString(row[1])
                }).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }

          
        }

        public List<ModeloVenta> ListaReporteVenta(string fechainicio, string fechafin)
        {
            List<clsParametro> lst = new List<clsParametro>();
            try
            {
                lst.Add(new clsParametro("@fechainicio", string.IsNullOrEmpty(fechainicio) ? "" : fechainicio));
                lst.Add(new clsParametro("@fechafin", string.IsNullOrEmpty(fechafin) ? "" : fechafin));


                return (from DataRow row in clsConexion.Instancia.Listado("sp_reporte_ventas", lst).Rows
                        select new ModeloVenta
                        {

                            codigo_venta_cab = Convert.ToString(row[0]),
                            nombre_cliente = Convert.ToString(row[1]),
                            cantidad = Convert.ToInt32(row[2]),
                            Total_venta = Convert.ToDecimal(row[3]),
                            vendedor = Convert.ToString(row[4]),
                            fecha_venta = Convert.ToString(row[5]),
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ModeloStock> ListaReporteStock(string fechainicio, string fechafin, string articulo)
        {
            List<clsParametro> lst = new List<clsParametro>();
            try
            {
                lst.Add(new clsParametro("@nombreArticulo", string.IsNullOrEmpty(articulo) ? "" : articulo));
                lst.Add(new clsParametro("@fechainicio", string.IsNullOrEmpty(fechainicio) ? "" : fechainicio));
                lst.Add(new clsParametro("@fechafin", string.IsNullOrEmpty(fechafin) ? "" : fechafin));


                return (from DataRow row in clsConexion.Instancia.Listado("sp_reporte_stock", lst).Rows
                        select new ModeloStock
                        {
                            codigo_articulo = Convert.ToInt32(row[0]),
                            codigo_marca    = Convert.ToInt32(row[1]),
                            serie           = Convert.ToString(row[2]),
                            nombre_aritculo = Convert.ToString(row[3]),
                            descripcion     = Convert.ToString(row[4]),
                            costo           = Convert.ToDecimal(row[5]),
                            stock           = Convert.ToInt32(row[6]),
                            fecha_articulo  = Convert.ToString(row[7]),
                            estado          = Convert.ToString(row[8]),
                            id_usuario      = Convert.ToInt32(row[9])

                            //codigo_venta_cab = Convert.ToString(row[0]),
                            //nombre_cliente = Convert.ToString(row[1]),
                            //cantidad = Convert.ToInt32(row[2]),
                            //Total_venta = Convert.ToDecimal(row[3]),
                            //vendedor = Convert.ToString(row[4]),
                            //fecha_venta = Convert.ToString(row[5]),
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ModeloInformacionUsuario ObtenerInfoPersona(string codpersona)
        {
            throw new NotImplementedException();
        }
    }
}
