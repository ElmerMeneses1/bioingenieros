﻿using LOGICA.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA.Interface
{
    public interface IRepositorioListados
    {

        ModeloInformacionUsuario ObtenerInfoPersona(string codpersona);
        List<ModeloComboPais> ComboPais();
        List<ModeloVenta> ListaReporteVenta(string fechainicio, string fechafin);
        List<ModeloStock> ListaReporteStock(string fechainicio, string fechafin, string articulo);
    }
}
