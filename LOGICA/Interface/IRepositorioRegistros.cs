﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA.Interface
{
    public  interface IRepositorioRegistros
    {
        string RegistrarUsuario(string nombre, string apellido, string email, string usuario, string clave, int pais);
    }
}
