﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA.Modelo
{
    public class ModeloInformacionUsuario
    {

        public  string nombre { get; set; }
        public string apellidos { get; set; }
        public string tipodocumento { get; set; }
        public string numdocumento { get; set; }
        public string telefono { get; set; }
        public string direcciones { get; set; }
        public string idpais { get; set; }
        public string pais { get; set; }
        public string estado { get; set; }
    }
}
