﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA.Modelo
{
    public class ModeloStock
    {
	public int codigo_articulo {get;set;}
	public int codigo_marca {get;set;}
	public string serie {get;set;}
	public string nombre_aritculo {get;set;}
	public string descripcion {get;set;}
	public decimal costo {get;set;}
	public int stock {get;set;}
	public string fecha_articulo {get;set;}
	public string estado {get;set;}
	public int id_usuario { get; set; }
	}
}
