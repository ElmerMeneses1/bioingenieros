﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOGICA.Modelo
{
   public class ModeloVenta
    {

        public string codigo_venta_cab { get; set; }
        public string nombre_cliente { get; set; }
        public int cantidad { get; set; }
        public decimal Total_venta { get; set; }
        public string vendedor { get; set; }
        public string fecha_venta { get; set; }

    }
}
