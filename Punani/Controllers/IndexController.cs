﻿using LOGICA.Interface;
using LOGICA.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Punani.Controllers
{
    public class IndexController : Controller
    {

        private IRepositorioListados _repostioriolistados;
        private IRepositorioRegistros _repositorioRegistros;

        // GET: RegistroExpediente

        public IndexController()
        {
            if (_repostioriolistados == null)
            {
                _repostioriolistados = new RepositorioListado();
            }

            if (_repositorioRegistros == null)
            {
                _repositorioRegistros = new RepositorioRegistros();
            }

        }
        // GET: Index
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReporteVenta()
        {
            return View();
        }

        public ActionResult ReporteStock()
        {
            return View();
        }

        public JsonResult ListaReporteVenta(string xfechainicio, string xfechafin)
        {
            var lista = _repostioriolistados.ListaReporteVenta( xfechainicio ,  xfechafin);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListaReporteStock(string xfechainicio, string xfechafin, string xarticulo)
        {
            var lista = _repostioriolistados.ListaReporteStock(xfechainicio, xfechafin, xarticulo);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }
    }
}