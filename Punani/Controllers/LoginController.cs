﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using LOGICA.Interface;
using LOGICA.Modelo;
using LOGICA.Repositorio;
using Punani.Models;

namespace Punani.Controllers
{
    public class LoginController : Controller
    {

        private IRepositorioLogin _repositoriologin;
        private IRepositorioListados _repostioriolistados;
        private IRepositorioRegistros _repositorioRegistros;



        public LoginController()
        {
            if(_repositoriologin == null)
            {
                _repositoriologin = new RepositorioLogin();
            }

            if (_repostioriolistados == null)
            {
                _repostioriolistados = new RepositorioListado();
            }

            if (_repositorioRegistros == null)
            {
                _repositorioRegistros = new RepositorioRegistros();
            }
        }


        // GET: Login
        public ActionResult Login()
        {

            return View();
        }



        [HttpPost]
        public JsonResult LoginPost( string usuario, string clave )
        {
                var respuesta = new ResponseModel
                {
                    respuesta = true,
                    redirect = "/Acceso/Login",
                    mensaje = "",
                    error = 0
                };

                    try
                    {
                        string mensaje = _repositoriologin.IniciarSession(usuario,clave, Request.UserHostAddress);

                        if (mensaje == "0")
                        {
                            respuesta.mensaje = "";
                            respuesta.error = 0;
                            respuesta.redirect = "/Index/Index";

                            //var datos = _repostioriolistados.ObtenerInfoPersona(usuario);
                            //if (datos != null)
                            //{
                            //    Session["nombre"] = datos.nombre;
                            //    Session["apellido"] = datos.apellidos;
                            //    Session["tipodocumento"] = datos.tipodocumento;
                            //    Session["numdocumento"] = datos.numdocumento;
                            //    Session["telefono"] = datos.telefono;
                            //    Session["direcciones"] = datos.direcciones;
                            //    Session["idpais"] = datos.idpais;
                            //    Session["pais"] = datos.pais;
                            //    Session["estado"] = datos.estado;
                            //}
                        }

                        else if (mensaje == "2")
                        {
                            respuesta.respuesta = true;
                            respuesta.mensaje = "Usted ya cuenta con una Sesion Abierta";
                            respuesta.error = 2;
                           Session["usuario"] = usuario;
                        }

                        else
                        {
                            respuesta.respuesta = false;
                            respuesta.mensaje = mensaje;
                            respuesta.error = 1;
                        }
           
                    }
                    catch(Exception ex)
                    {
                        respuesta.respuesta = false;
                        respuesta.mensaje = ex.Message;
                        respuesta.error = 2;
                    }

                    return Json(respuesta);
        }


        public ActionResult RegistrarUsuario()
        {
            return View();
        }


        public JsonResult RegistrarUsuarioPost(string nombre, string apellido ,string email, string usuario, string clave,int pais)
        {
            string mensaje = "", returnMail="";
            string fecha = DateTime.Now.ToString();

            mensaje = _repositorioRegistros.RegistrarUsuario(nombre, apellido, email, usuario, clave, pais);

            returnMail = EnviarCorreo(email, "Envío Automático", "<table bgcolor =\"#2B66A9\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-radius:10px 10px 0 0;border:1px solid #165796\">" +
                          "<tbody><tr><td style =\"padding:10px\"><p style=\"color:white;\">PUNANI</p></td>" +
                          "</tr></tbody></table><table bgcolor =\"#FFFFFF\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"margin:0 auto;padding:10px 10px;border-radius:0 0 10px 10px;border:1px solid #dddddd\"><tbody><tr><td style =\"padding-bottom:8px;font-size:22px;font-family:Helvetica,Arial,sans-serif;color:#f37c2f;font-weight:" +
                          "200\">Bienvenido a PUNANI " + nombre + apellido +  " </td></tr><tr><td style =\"padding-bottom:8px;font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#737373;font-weight:normal\"></td></tr><tr><td style =\"padding-bottom:8px;font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#737373;font-weight:normal\"><span style=\"font-weight:" +
                          "200;color:#165796;margin-right:5px\"><br>Usuario:</span>" + usuario + "</td></tr><tr><td style =\"padding-bottom:8px;font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#737373;font-weight:normal\"><span style=\"font-weight:200;color:#165796;margin-right:5px\">clave:</span>" + clave + "<br></td></tr><tr>" +
                          "<td style=\"padding-bottom:8px;font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#737373;font-weight:normal\"><span style=\"font-weight:200;color:#165796;margin-right:5px\">" + fecha + "</td></tr><td style =\"padding-bottom:8px;font-size:12px;line-height:16px;font-family:Helvetica,Arial,sans-serif;color:#165796;font-weight:normal\"><br><br></td></tr><tr><td style =\"padding-top:43px;padding-bottom:8px;font-size:12px;line-height:" +
                          "16px;font-family:Helvetica,Arial,sans-serif;color:#165796;font-weight:normal;border-top:1px dashed #dddddd\"><span style=\"font-weight:bold\">Alerta!!!</span><br>Si tienes alguna duda comunicarse con la municipalidad, este correo es automático y no acepta respuestas.</td></tr></tbody></table>");
          

            return Json(mensaje);
        }

        public string EnviarCorreo(string toEmail, string subject, string emailBody)
        {
            try
            {
                MailMessage correo = new MailMessage();
                correo.From = new MailAddress("dcom0419@outlook.com");
                correo.To.Add(toEmail);
                correo.Subject = subject;
                correo.Body = emailBody;
                correo.IsBodyHtml = true;
                correo.Priority = MailPriority.High;

                SmtpClient smtp = new SmtpClient();
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential("dcom0419@outlook.com", "Spiderman123");
                smtp.EnableSsl = true;
                smtp.Host = "smtp.live.com";
                smtp.Port = 587;
                smtp.Send(correo);
                correo.Dispose();

                return "Correcto";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [HttpPost]
        public JsonResult ComboPais()
        {
            List<ModeloComboPais> pais = new List<ModeloComboPais>();
            pais = _repostioriolistados.ComboPais();
            return Json(pais);
        }



        public JsonResult EliminarIP()
        {
            string mensaje = "";
            string cod = Session["usuario"].ToString();
            mensaje = _repositoriologin.EliminarIP(cod);
            return Json(mensaje);
        }


    }
}